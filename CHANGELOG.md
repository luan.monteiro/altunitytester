# Changes in version 1.6.2
## New
- "Publish altUnity on nuget" !328  
## Bug fixes and improvements

- "3D object disappears when highlighted for screenshot" !349  
- "Add possibility to call a method from an object inside an gameObject" !348  
- "parentId is wrong for canvas objects" !341  
- "Click event is not triggered when swipe is moving but remains on the same object" !347  
- "Wrong shaders are assigned to materials after object is highlighted" !346  
- "call CreateJsonFileForInputMappingOfAxis during build from commandline" !343  
- "Return value for CallStaticMethod is wrong in documentation" !342  
- "Handle case when there is no camera selecting object from screenshot" !318  
- "Keypress moves the character 2 positions instead of 1" !337  
- "Remove tests requiring moq.dll from unitypackage" !340  
- "AltElement repr does not conform to the standard in python" !339  
- "Add flag to make sure AltUnity Tester can be added only to dev builds" !335  
- "Add a note to documentation regarding that only dev builds should have AltUnity Tester" !334  
- "Spelling mistake in AltUnityTester UI and documentation." !333  
- "Add an overview to License section, so users are not confused what restrictions they have" !332  
- "Port forwarding text has different color in dark theme than other text" !329  
- "Iterate enabled cameras on HightlightObjectFromCoordinatesCommand" !330  
- "Fix inspector documentation link" !326  
- "Add documentation for selecting scenes when building the game"!325  
- "Update AltUnity Tester documentation structure" !324  

## Contributors 
- Dorin Oltean @dorinaltom
- Gombos Kriszta @GombosKriszta 
- Robert Poienar @robert_poienar

# Changes in version 1.6.1

## Bug fixes and improvements
- Solved memory problems cause by Get/SetPixels !320
- Removed annoying Debug.LogError() !317
- Fixed logging exceptions !316
- Added link to AltUnityInspector !321

## Contributors 
- Dorin Oltean @dorinaltom
- Robert Poienar @robert_poienar

# Changes in version 1.6.0

## Bug fixes and improvements

- "Remove deprecated methods" !301 
- "Fix AltUnitySyncCommand to receive until message id matches" !311 
- AltUnityTapCommand. Add clickTime !288 
- "GetAllLoadedScenesCommand return scene by build index and not by loaded scenes" !309 
- Fix bug in GetAllLoadedScene !291 
- "String is not considerate a primitive" !307 
- "swipe and wait does not wait the expected duration because of float conversion" !306 
- "add message id to driver server communication" !303 
- "Input actions are not finished when timeScale is 0" !302 
- "Wrong scale difference/original size of screenshot is sent" !300  
- "Change AltUnityProperty and AltUnityField to be the same and add type field" !297 
- "Add more control what fields should return get_all_fields" !267 
- "Pressing mouse button doesn't click on buttons" !296 
- "Display an error message If the port AltUnityServer listens to is used by another process" !295 
- "If no size value is given for screenshot command then it should return full size screenshot" !294 
- "Method to GetAllElements with minimal information" !293  
- "GetComponentProperty sends errorNullRefferenceMessage instead of errorComponentNotFoundMessage" !292 
- "Add try catch when transforming game object to altunityobject" !287 
- "GetMemberForObjectComponent doesn't recognize all properties or fields" !286 
- "Change command name from FindObjectWhichContains to FindObjectWhichContain" !264   
- "Update retry logging in unity in AltUnityDriver" !275 
- "Update gitignore to ignore more files that do not need to be in the repo" !284    
- Update documentation

## Contributors 
- Dorin Oltean @dorinaltom
- Ru Cindrea @ru.cindrea
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru
- Raluca Vaida @raluca.vaida 
- Gombos Kriszta @GombosKriszta 
- Andra Cardas @andra.cardas 
- Robert Poienar @robert_poienar
- SivanYakir @SivanYakir 
- Ka3u6y6a @ka3u6y6a 

# Changes in version 1.5.7

## Bug fixes and improvements


- "Remove null checker to make compatible with .net 3.x" !268
- "GetVersion should return server version when there is a mismatch" !266  
- "Load scene additive" !265
- Resolve "AltUnityDriver python does not display warnings by default" !263
- Update documentation

## Contributors 
- Dorin Oltean @dorinaltom
- Ru Cindrea @ru.cindrea
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru
- Dorel @doreln 
- Bogdan Birnicu @bogdan.birnicu
- Raluca Vaida @raluca.vaida 
- Gombos Kriszta @GombosKriszta 
- Andra Cardas @andra.cardas 
- Robert Poienar @robert_poienar

# Changes in version 1.5.6

## Bug fixes and improvements

- "Make "Drag" and "Drop" command deprecated" !259
- "Make AltUnityDriver interface consistent across all language clients" !257
- "Add option to specific camera other than name in find objects commands" !207
- "Python - iOS - exception still thrown if we try to connect but server is not yet running" !255  
- "click_element() not working in python" !254  
- "Multiple warning messages displayed when using the editor" !252  
- "Update documentation for overview and get started" !251  
- "Test List doesn't appear when configuration page is created" !250  
- "Double clicking on a test in AltUnity Tester window doesn't open the test file in IDE" !249  
- "Make the Android / iOS options under Platform more clear" !248
- "Tilt command not working" !247  
- Exposing the AltBaseSettings to support users to write custom commands. !253  
- "AltElement in Python doesn't have parentId property" !238  
- "Class name is checked although not all tests under it are checked (in the Tests list)" !237  
- "Scroll command not functioning" !246  
- "No relevant error message and build is not made when having empty "Output path"" !244  
- "General Method to run c# test in command line" !242  
- "Examples are missing from unity package that is in documentation" !239  
- "Continuous error triggered when making changes to the test file while the game is running" !245  
- ""NullReferenceException" displayed as info message after running tests" !243  
- "Finding parent by path gives "failedToParseMethodArguments"" !228  
- "Finding object by component should accept component name just like other methods" !241  
- "Handle null exception in case object has null components(invalid scripts)" !208  
- Added get_all_fields_of() for Python along with Typing help !232  
- Test List Improvements !240  
- Added get_all_components() for Python along with Typing help !231  
- "AltUnityTesterEditorSettings should not appear in the project files" !236  
- "Update example tests in order to avoid possible naming conflicts" !233  
- "Rename some of the server commands all to respect the same pattern" !235  
- "Move "Run tests" section below "Build" and "Run" sections" !234  
- "Add example in documentation for function that are missing" !209  
- Changed all the is to the ==, where python complained !230

## Contributors

- Robert Poienar @robert_poienar
- Dorin Oltean @dorinaltom
- Ru Cindrea @ru.cindrea
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru
- Dorel @doreln 
- Bogdan Birnicu @bogdan.birnicu
- Raluca Vaida @raluca.vaida 
- Mike Talalaevskiy @Day0Dreamer
- Miguel Ibero @miguel.ibero
- dcole-gsn @dcole-gsn 

# Changes in version 1.5.5

## Bug fixes and improvements

- Resolve "Create in sample scene a button that load other scene and test it" !221 
- Resolve "Add a way to control the amount of output" !222 
- Resolve "Add option to set port in BuildGame Method and move to Awake runner instantiation settings" !213 
- Resolve "Compile errors after adding 'ALTUNITYTESTER' in scripting define symbols" !210 
- Resolve "Add Text and TextMeshPro Text component to SetText" !220 
- Resolve "Add favicon to altom.gitlab.io" !223 
- Resolve "If the server is not started when calling getServerVersion, it's stuck" !217 
- Resolve "Add license information in documentation" !219 
- Resolve "Add google groups links in documentation" !215 
- Resolve "Move Bindings folder from BindingAndExample Folder" !214 


## Contributors

- Robert Poienar @robert_poienar
- Ru Cindrea @ru.cindrea
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru
- Mike Talalaevskiy @Day0Dreamer

# Changes in version 1.5.4
## New

- Double tap method for AltUnityObject !190 
- Add TapCustom command !194 
- "Add AltUnity to Maven" !204 

## Bug fixes and improvements

- "C# receive method adds additional characters in the response" !206 
- "Some elements to be highlighted are not found" !205 
- "Screenshot with object highlighted not working for camera" !196 
- "Return the object with the screenshot when given x y coordinates" !201 
- "Add Tank Sample to documentation" !191 
- "Change function name in python from click_Event to click_event" !200 
- "Error when uploading to pypi the latest version" !193 
- "Modify check version for earlier version that don't have the command to not throw an error" !199 
- "Sort better the order how method are returned from getAllMethods command" !198 
- "Make input visualizer pulsate when clicking" !189 
- "Add information about communication between server and driver" !197 
- "Add a note to Documentation about not having to rebuild the game when tests are changed" !192 
- "Are there similar methods for HoldButton , HoldButtonAndWait and Send_keys in Python binding?" !184
- "Add javadoc to AltUnity tester" !203 

## Contributors

- Robert Poienar @robert_poienar
- Ru Cindrea @ru.cindrea
- Ka3u6y6a @ka3u6y6a
- Andrei Ionut Benyi @ionut.benyi
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru


# Changes in version 1.5.3
## New

- Add methods for swipe by more then two points !173 
- Server needs GetVersion() command" !172 

## Bug fixes and improvements

- Move tests that search for multiple object to another scene" !185 
- Update ReadMe from project" !186 
- Add java package in pages and bindings" !183 
- Test in Python doesn't run at all" !177 
- Error thrown for object name when Encoding/Decoding" !178 
- Implement static `removePortForwarding` methods so that they don't remove all other existing ports that are forwarded" !167 
- Add link to java examples documentation" !182 
- adb path should be configurable on windows and should be part of Android (not iOS) Settings" !179 
- Python bindings: missed install requirement - "deprecated" package" !180 
- Add documentation for port forwarding for iOS and Android" !160 
- Change class names to have prefix alt|altunity" !176 
- If extension is not define in output path it will be set automatically !156 
- AltUnityPopCanvas set to be in front and option to deactivate" !175 
- Input axis not working if it not set all four values" !174 
- Bug: null reference returned for object where parent is canvas with world space and no camera set" !169 
- Update appium tests" !170 
- Python bindings - port forwarding cannot be done if Appium is not used" !159 
- HighlightSelectedObjectCommand should return "Ok" not "null"" !168 
- Refresh port forwarding throws error" !166 
- SetText doesn’t work for TextMeshPro components" !164 
- DeviceId overlapping with Local Port Id" !165 

## Contributors

- Robert Poienar @robert_poienar
- Ru Cindrea @ru.cindrea
- Ka3u6y6a @ka3u6y6a
- Andrei Ionut Benyi @ionut.benyi
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru

# Changes in version 1.5.2

## New

- Add option to activate input visualizer from AltUnityTester GUI !145
- Add options to disable pop-up from GUI !154
- Screenshot functionality for all 3 drivers !140

## Bug fixes and improvements

- Find Objects doesn't find all objects !144
- ALTUNITYTESTER define symbol is not inserted at the right platform when playing in editor !155
- Add theme to documentation !147
- Error on python 2.7: TypeError super() takes at least 1 argument (0 given) !146
- Made IP change to accept all incoming connections to AltUnityServer, at... !151
- Add gitter link to documentation !152
- Fix java Wait for object not be present !150
- Fix click at coordinates on python !149
- wait_for_object fails !142
- Fix documentation title and broken links

## Contributors

- Robert Poienar @robert_poienar
- Ru Cindrea @ru.cindrea
- Thejus Krishna
- Ka3u6y6a @ka3u6y6a
- Raluca Vaida @raluca.vaida
- pusoktimea @pusoktimea
- Alexandru Rotaru @alex.rotaru

# Changes in version 1.5.0

## Refactoring

- Modify C# driver not to depend on Unity !131 
- Project refactoring: Server !108 
- Project refactoring: AltDriver(python) !113 
- Project refactoring: AltDriver(Java) !115 

## New

- Add pop-up to AltUnityPrefab !106 
- Add clicks visualization !122 
- Add set text command !125
- Open file test when clicking on a test from window editor !118 
- Create documentation in form of a wiki !128 
- Get Server output !107 

## Bug fixes and improvements:

- Divide editor window to see better the content !119 
- The method callComponentMethod was constructing an AltUnityObjectAction with...!109 
- Server throw more generic error for getComponent than it used to !130 
- Add test for Java method to callComponentMethod with assembly name too !111 
- Correct obsolete texts in c# driver !123 
- Update readme with correct url to download latest Unity package !116 
- AltUnityTester - Create new AltUnity Test doesn't do anything !110 
- Add command for getting text from ui text and text mesh pro !120 
- Missing tests for getAllElement in python !134 
- Missing tests for FindObjectWhichContains!135 
- Add test for "inspector" commands c# !137 
- Change tests to be independent of the screen resolution !129 
- Missing command in c# driver: WaitForObjectWhichContains !124 

## Contributors

-  Robert Poienar @robert_poienar
-  Ru Cindrea @ru.cindrea
-  Ka3u6y6a @ka3u6y6a
-  ricardo larrahondo @ricardorlg
 
# Changes in version 1.4.0

## Keyboard, joystick and mouse simulation
- Add keyboard input simulation !98 
- Joystick controls !102 

## Bug fixes and improvements:
- Modify the project to use AssemblyQualifiedName instead of just class name" !97 
- Make "Play in Editor" to open the first scene or to put the AltUnityTester in the active scene !100 
- Fix AltUnityTestWindow when using AssemblyDefinitionFiles !99 
- Add timeout to get_current_scene call during socket setup !94 
- "setup_port_forwarding" method in runner.py file from python bindings is throwing an exception !95 
- Set adbFileName in linux !93 
- Fix ClickOnScreenAtXy !92 
- driver.findElement is unable to find some elements in ver 1.3.0 !91 

## Contributors
-  Robert Poienar @robert_poienar
-  Ru Cindrea @ru.cindrea
-  Arthur @LordStuart 
# Changes in version 1.3.0

## Improvements to Unity Editor GUI / AltUnityTester Unity Test Runner
-  Connected devices and port forwarding status are shown in UI, more than one device can be used at a time  - !81
-  Command separator is configurable (from code and UI)  - !79
-  More build options - !77 
-  Improvements to test running finish dialog  - !75, !82
-  Ability to run tests in Editor - !55 

## Running in cloud
-  Added documentation and example for running on AWS Device Farm  - !20       
-  Fixed issue with port fowarding on iOS Python client that caused problems with running in Bitbar and AWS cloud - !76

## Overall Refactoring:
-  Split AltUnityTesterEditor and provide an interface for building a game with AltUnityTester on different platforms  - !71
-  Java refactoring and improvements - !60, !58 

## Release Artifacts:
-  Create deployment to pages  - !66  
-  Create AltUnityTester package for every branch via CI pipeline  - !65        

## Bug fixes and improvements:
-  WaitForElementToNotBePresent doesn't work correctly  - !89 
-  IdProject is not created when Assets is not parent for AltUnityTester folder  - !87
-  Change appium methods duration variable name to be suggestive  - !83
-  NullREferenceException is returned when calling tapScreen twice  - !74
-  AltUnityRunnerPrefab is not removed when  Run in Editor   - !73      
-  Tester should work without any camera in the scene  - !72       
-  Run failed test or selected will run all the test  - !69       
-  Driver froze when the information receive is a certain size  - !70       
-  Disable objects not found by FindObjects  - !68       
-  Make AltUnityTester work with Unity 2017.x  - !64
-  Fix links in Java bindings readme - !62   
-  Update for Python Bindings runner.py - !61   


## New AltUnityRunner commands and features:
-  (C# only) GetScreenshot command - !86, !78, !56, !54
-  Add way to find elements that are not enabled - !50
-  Add GetAllObjects(), GetAllMethods(), GetAllComponents, GetAllScenes(), GetAllProperties() commands, add way to find objects by IDs - !63, !59, !53, !51
-  Add command to set Time.timeline  - !85  

## Contributors
-  Robert Poienar @robert_poienar
-  Ru Cindrea @ru.cindrea
-  Kamil @kaszarek
-  Nikita Ershov @ershov1
-  Napster @napstr
