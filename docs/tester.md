# AltUnity Tester

AltUnity Tester is an open-source UI driven test automation tool that helps you find objects in your game and interacts with them using tests written in C#, Python or Java. You can run your tests on real devices (mobile, PCs, etc.) or inside the Unity Editor.

```eval_rst

.. toctree::
   :caption: Table of contents:
   :maxdepth: 3
   
   pages/overview
   pages/get-started
   pages/altunity-tester-editor
   pages/tester-with-appium
   pages/tester-with-cloud
   pages/examples
   pages/commands
   pages/advanced-usage
   pages/license
   pages/contributing
