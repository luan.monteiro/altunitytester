package ro.altom.altunitytester.altUnityTesterExceptions;

public class CouldNotPerformOperationException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 1466903824560323790L;

    public CouldNotPerformOperationException() {

    }

    public CouldNotPerformOperationException(String message) {
        super(message);
    }
}
