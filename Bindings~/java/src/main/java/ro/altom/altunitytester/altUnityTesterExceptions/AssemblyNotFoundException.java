package ro.altom.altunitytester.altUnityTesterExceptions;

public class AssemblyNotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -755832878650330976L;

    public AssemblyNotFoundException() {
    }

    public AssemblyNotFoundException(String message) {
        super(message);
    }
}
