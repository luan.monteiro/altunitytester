package ro.altom.altunitytester;

public class AltUnityDriverParams {

    public String ip = "127.0.0.1";
    public int port = 13000;
    public String requestSeparator = ";";
    public String requestEnd = "&";
    public Boolean logEnabled = false;
    public int connectTimeout = 60;

}