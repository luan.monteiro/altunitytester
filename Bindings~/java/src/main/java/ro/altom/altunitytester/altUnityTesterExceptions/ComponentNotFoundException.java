package ro.altom.altunitytester.altUnityTesterExceptions;

public class ComponentNotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -221122585107928850L;

    public ComponentNotFoundException() {
    }

    public ComponentNotFoundException(String message) {
        super(message);
    }
}
