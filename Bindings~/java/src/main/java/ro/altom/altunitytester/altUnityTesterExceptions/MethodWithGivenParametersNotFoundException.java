package ro.altom.altunitytester.altUnityTesterExceptions;

public class MethodWithGivenParametersNotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 5808178384586672058L;

    public MethodWithGivenParametersNotFoundException() {
    }

    public MethodWithGivenParametersNotFoundException(String message) {
        super(message);
    }
}
