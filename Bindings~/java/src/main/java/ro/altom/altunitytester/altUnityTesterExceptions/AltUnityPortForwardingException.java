package ro.altom.altunitytester.altUnityTesterExceptions;

public class AltUnityPortForwardingException extends AltUnityException {

    /**
     *
     */
    private static final long serialVersionUID = -7629828251460910071L;

    public AltUnityPortForwardingException() {
    }

    public AltUnityPortForwardingException(String message) {
        super(message);
    }

    public AltUnityPortForwardingException(Throwable exception) {
        super(exception);
    }

    public AltUnityPortForwardingException(String message, Throwable exception) {
        super(message, exception);
    }
}
